import React from 'react';
import logo from './logo.svg';
import './App.css';

import Tree from './views/components/Tree/Tree';

import { treeData } from './mocks';

function App() {
  return (
    <div className="App">
      <Tree treeData={treeData} />
    </div>
  );
}

export default App;
