export const treeData = [{
    "id": 1,
    "name": "0-0-0-0-0-0",
    "children": [
        {
            "id": 2,
            "name": "1-1-1-1-1-1"
        },
        {
            "id": 3,
            "name": "1-1-1-1-1-1", 
            "children": [
                {
                    "id": 4,
                    "name": "2-2-2-2-2-2"
                }
            ]
        }
    ]
}]