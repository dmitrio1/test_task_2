import React from 'react'
import PropTypes from 'prop-types'
import { FaFolder, FaFile, FaChevronDown, FaChevronRight } from 'react-icons/fa'

const TREE_PADDING_STEP = 20;

const TreeNode = props => {
    const { treeData, isExpanded, depth = 1, handleClick } = props;
    const { id, name, children } = treeData;


    const getTreeStyle = (depth, step) => ({
        paddingLeft: depth * step + 'px'
    });

    const hasChildren = (treeItem) => Array.isArray(treeItem.children) && treeItem.children.length > 0;

    const getTreeIcon = (treeItem) => {
        let icon;
        const itemHasChildren = hasChildren(treeItem);
        const itemIsExpanded = isExpanded(treeItem.id);

        if (!itemHasChildren) {
            icon = <FaFile color='#5c669e' />
        }
        else if (itemIsExpanded) {
            icon = <>
                <FaChevronDown color='#5c669e' />
                <FaFolder color='#5c669e' />
            </>;
        }
        else {
            icon = <>
                <FaChevronRight color='#5c669e' />
                <FaFolder color='#5c669e' />
            </>;
        }
        return icon;
    }

    const icon = getTreeIcon(treeData);

    return (
        <>
            <span onClick={handleClick(id)}>
                {icon}
                {name}
            </span>
            <ul style={getTreeStyle(depth, TREE_PADDING_STEP)}>
                {
                    children && isExpanded(id) && children.map(c => {
                        return <TreeNode
                            key={c.id}
                            treeData={c}
                            depth={depth + 1}
                            isExpanded={isExpanded}
                            handleClick={handleClick}
                        />
                    })
                }
            </ul>

        </>
    )
}

TreeNode.propTypes = {

}

export default TreeNode
