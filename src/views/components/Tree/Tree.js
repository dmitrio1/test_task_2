import React, { useState } from 'react';
import PropTypes from 'prop-types';


import TreeNode from '../TreeNode';
import styles from './tree.module.scss';

const Tree = props => {

    const { treeData } = props;
    const [expanded, setExpanded] = useState([]);

    const collapseAll = () => {
        setExpanded([]) 
    };

    const getChildrenIds = (treeData) => {
     
        const cb = (acc, item) => {
            let res = acc.concat(item.id);
            return (item.children || []).reduce(cb, res);
        };

        return treeData.reduce(cb, []);
    };

    const expandAll = () => {
        const allIds = getChildrenIds(treeData);
        setExpanded(allIds);
    }

    // const isRoot = (depth) => depth === 0;

    const isExpanded = (id) => expanded.indexOf(id) !== -1 ? true : false;

    const handleClick = (id) => (e) => {
        if (!isExpanded(id)) {
            setExpanded([...expanded, id])
        }
        else {
            setExpanded(expanded.filter(item => item !== id))
        }
    }

    return (
        <div className={styles.root}>
            {
                <div>
                    <button onClick={expandAll}> Expand All </button>
                    <button onClick={collapseAll}> Collapse All </button>
                </div>
            }
            <ul>
                {
                    Array.isArray(treeData) && treeData.map(td => {
                        return (
                            <li key={td.id} className={styles.treeItem} >
                                <TreeNode 
                                    treeData={td} 
                                    isExpanded={isExpanded} 
                                    handleClick={handleClick}
                                />
                            </li>
                        )
                    }
                    )
                }
            </ul>
        </div>
    )
}

Tree.propTypes = {
    treeData: PropTypes.arrayOf(PropTypes.object).isRequired,
}

export default Tree;
